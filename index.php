<?php
require 'Slim/Slim.php';

\Slim\Slim::registerAutoloader();

$app = new \Slim\Slim();

$response = $app->response();
$response->header('Access-Control-Allow-Origin', '*');

$app->get('/etc/:year/:month', 'getEtcUser1');
$app->get('/etc2/:path_type/:portal_type/:year/:month', 'getEtcUser2');
$app->get('/portal/:year/:month', 'getPortalUser');
$app->get('/path/:year/:month', 'getPathUser');
$app->get('/period/:year/:month', 'getPeriodUser');

// $app->get('/wines/:id', 'getWine');
// $app->get('/wines/search/:query', 'findByName');
// $app->post('/wines', 'addWine');
// $app->put('/wines/:id','updateWine');
// $app->delete('/wines/:id', 'deleteWine');

$app->run();


//년도,월 선택 시 막대그래프데이터 get
function getPeriodUser($year, $month) {
	if(1<=$month&&$month<10){
		$month = "0".$month;
	}

	// mysqli 트랜잭션
	$link = mysqli_connect("www.3030eng.com","root","Rakgk5go!~","home3030");
	if(mysqli_connect_errno()){
		printf("Connect failed: %s",mysqli_connect_errno());
		exit();
	}
	mysqli_begin_transaction($link);


	//년도만 넘어온 경우
	if($year!='no'&&$month=='no'){
		//월별집계(중복ip제거)
		$result = mysqli_query($link, "SELECT left(postdate,7), COUNT(distinct userip) FROM homepagelog WHERE left(postdate,4)='". $year ."'GROUP BY left(postdate,7)");
		mysqli_commit($link);

		//각 변수들 선언 및 카운팅
		$period_arr = [];
		//배열초기화
		for($i=0; $i<12; $i++){
			$period_arr[$i]=0;
		}

		//데이터존재하는 시작"달" 찾기. 시작달 이전 달는 모두 0으로 초기화 과정
		$row = mysqli_fetch_assoc($result);
		$month = (int)$row["left(postdate,7)"][5].$row["left(postdate,7)"][6];
		for($i=0; $i<$month-1; $i++){
			$period_arr[$i]=0;
		}
		$period_arr[$month-1]=(int)$row["COUNT(distinct userip)"];
		

		//시작"일"다음 날 부터 이제 $period_arr에 값 차곡차곡 쌓아주기
		while($row = mysqli_fetch_assoc($result)){
			$month++;
			//디비에서 가져올 달을 중간에 빼먹고 가져온 경우 처리(해당달에 값이 없을 경우 값 안가져옴)
			while($month!=(int)$row["left(postdate,7)"][5].$row["left(postdate,7)"][6]){
				$period_arr[$month]=0;
				$month++;
			}
			$period_arr[$month-1]=(int)$row["COUNT(distinct userip)"];
		}
	}


	//년도와 월 모두 넘어온 경우
	else if($year!='no'&&$month!='no'){
			//월별집계(중복ip제거)
		$result = mysqli_query($link, "SELECT left(postdate,10), COUNT(distinct userip) FROM homepagelog WHERE left(postdate,7)='".$year."-".$month."' GROUP BY left(postdate,10)");
		mysqli_commit($link);

		//각 변수들 선언 및 카운팅
		$period_arr = [];
		//배열초기화..2월은 29일, 3월은 31일, 이런식으로 배열생성해야하기 때문
		if($month==1||$month==3||$month==5||$month==7||$month==8||$month==10||$month==12){
			for($i=0; $i<31; $i++)
				$period_arr[$i]=0;
		}
		else if($month==4||$month==6||$month==9||$month==11){
			for($i=0; $i<30; $i++)
				$period_arr[$i]=0;
		}
		else if($month==2){
			for($i=0; $i<29; $i++)
				$period_arr[$i]=0;
		}

		//데이터존재하는 시작"일" 찾기. 시작일 이전 날짜는 모두 0으로 초기화 과정
		$row = mysqli_fetch_assoc($result);
		$day = (int)$row["left(postdate,10)"][8].$row["left(postdate,10)"][9];
		for($i=0; $i<$day-1; $i++){
			$period_arr[$i]=0;
		}
		$period_arr[$day-1]=(int)$row["COUNT(distinct userip)"];
		
		//시작"일"다음 날 부터 이제 $period_arr에 값 차곡차곡 쌓아주기
		while($row = mysqli_fetch_assoc($result)){
			$day++;
			//디비에서 가져올 날짜가 중간에 빼먹고 가져온 경우 처리(해당날짜에 값이 없을 경우 값 안가져옴)
			while($day!=(int)$row["left(postdate,10)"][8].$row["left(postdate,10)"][9]){
				$period_arr[$day]=0;
				$day++;
			}
			$period_arr[$day-1]=(int)$row["COUNT(distinct userip)"];
		}
	}
	
	/*key값 없이 JSON데이터에 담는 방법(user_for_etc.js에서 바로 인덱스 접근해서 테이블에 뿌려주기에 용이하다~)*/
	// $arr = array($Jan, $Feb ,$Mar ,$Apr ,$May ,$Jun ,$Jul ,$Aug ,$Sep ,$Oct ,$Nov ,$Dec ,$Etc);

	echo '{"period": ' . json_encode($period_arr) . '}';

}

function getPortalUser($year, $month){
	/*Path던..Portal이던.. 1월+2월+3월..수 =전체기간수가 왜 다른가? ->쿼리에서 중복된걸 제거하는데.. 달이 달라지면 중복된걸 뺴주지 못하고 다시 카운트 하기 때문(쿼리를 새롭게 날리기 떄문에 DISTINCT가 중복을 잡아내지 못한다.. )*/
	if(1<=$month&&$month<10){
		$month = "0".$month;
	}
	// mysqli 트랜잭션
	$link = mysqli_connect("www.3030eng.com","root","Rakgk5go!~","home3030");
	if(mysqli_connect_errno()){
		printf("Connect failed: %s",mysqli_connect_errno());
		exit();
	}
	mysqli_begin_transaction($link);

	//각 변수들 선언 및 카운팅
	$portal_arr= array();
	array_push($portal_arr,'portal_naver');
	array_push($portal_arr,'portal_daum');
	array_push($portal_arr,'portal_google');
	array_push($portal_arr,'portal_etc');
	array_push($portal_arr,'total_cnt');

	$query_str_arr=array();
	//년도, 월 선택 안된 경우(전체기간)
	//referer 필드에서 naver를 포함하는 데이터의 갯수(중복제거)
	$query_str_arr[0]="SELECT count(distinct referer) FROM homepagelog WHERE referer LIKE '%naver%'";
	//referer 필드에서 daum을 포함하는 데이터의 갯수(중복제거)
	$query_str_arr[1]="SELECT count(distinct referer) FROM homepagelog WHERE referer LIKE '%daum%'";
	//referer 필드에서 google을 포함하는 데이터의 갯수(중복제거)
	$query_str_arr[2]="SELECT count(distinct referer) FROM homepagelog WHERE referer LIKE '%google%'";
	//referer 필드에서 기타경로 데이터의 갯수(중복제거)
	$query_str_arr[3]="SELECT count(distinct referer) FROM homepagelog WHERE referer NOT LIKE '%naver%' AND referer NOT LIKE '%daum%' AND referer NOT LIKE '%google%'";
	//referer 필드에서 총 데이터의 갯수(중복제거)
	$query_str_arr[4]="SELECT count(distinct referer) FROM homepagelog";
	// }

	//년도만 선택된 경우
	if($year!='no' && $month=='no'){
		for($i=0; $i<count($query_str_arr); $i++){
			if($i==4){
				$query_str_arr[$i]=$query_str_arr[$i]. " WHERE left(postdate,4)='".$year."'";
				continue;
			}
			$query_str_arr[$i]=$query_str_arr[$i]. " AND left(postdate,4)='".$year."'";
		}
	}

	//년도, 월 모두 선택된 경우
	if($year!='no' && $month!='no'){
		for($i=0; $i<count($query_str_arr); $i++){
			if($i==4){
				$query_str_arr[$i]=$query_str_arr[$i]. " WHERE left(postdate,7)='".$year."-".$month."'";
				continue;
			}
			$query_str_arr[$i]=$query_str_arr[$i]. " AND left(postdate,7)='".$year."-".$month."'";
		}
	}


	for($i=0; $i<count($portal_arr); $i++){
		$result = mysqli_query($link, $query_str_arr[$i]);
		mysqli_commit($link);
		$row = mysqli_fetch_assoc($result);
		$portal_arr[$i] = (int)$row['count(distinct referer)'];
	}

	/*key값 없이 JSON데이터에 담는 방법(user_for_etc.js에서 바로 인덱스 접근해서 테이블에 뿌려주기에 용이하다~)*/
	$arr = array();
	for($i=0; $i<count($portal_arr);$i++){
		$arr[$i] = $portal_arr[$i];
	}
	echo '{"portal": ' . json_encode($arr) . '}';
}

function getPathUser($year, $month){
	/*Path던..Portal이던.. 1월+2월+3월..수 =전체기간수가 왜 다른가? ->쿼리에서 중복된걸 제거하는데.. 달이 달라지면 중복된걸 뺴주지 못하고 다시 카운트 하기 때문(쿼리를 새롭게 날리기 떄문에 DISTINCT가 중복을 잡아내지 못한다.. )*/
	if(1<=$month&&$month<10){
		$month = "0".$month;
	}
	// mysqli 트랜잭션
	$link = mysqli_connect("www.3030eng.com","root","Rakgk5go!~","home3030");
	if(mysqli_connect_errno()){
		printf("Connect failed: %s",mysqli_connect_errno());
		exit();
	}
	mysqli_begin_transaction($link);

	//각 변수들 선언 및 카운팅
	$path_arr= array();
	array_push($path_arr,'path_search');
	array_push($path_arr,'path_blog');
	array_push($path_arr,'path_cafe');
	array_push($path_arr,'path_mail');
	array_push($path_arr,'path_direct');
	array_push($path_arr,'total_cnt');

	$query_str_arr=array();


	// 년도, 월 선택 안된 경우(전체기간)
	//검색 통해 접속한 데이터의 갯수(중복제거)
	$query_str_arr[0]="SELECT count(distinct referer) FROM homepagelog WHERE (referer LIKE '%search.naver%' OR referer LIKE '%search.daum%' OR referer LIKE '%google%')";
	//블로그 통해 접속한 데이터의 갯수(중복제거)
	$query_str_arr[1]="SELECT count(distinct referer) FROM homepagelog WHERE (referer LIKE '%blog.naver%' OR referer LIKE '%blog.daum%')";
	//카페 통해 접속한 데이터의 갯수(중복제거)
	$query_str_arr[2]="SELECT count(distinct referer) FROM homepagelog WHERE (referer LIKE '%cafe.naver%' OR referer LIKE '%map.daum%')";
	//메일 통해 접속한 데이터의 갯수(중복제거)
	$query_str_arr[3]="SELECT count(distinct referer) FROM homepagelog WHERE (referer LIKE '%mail.naver%' OR referer LIKE '%mail2.daum%' OR referer LIKE '%mail.google%')";
	//직접 접속한 데이터의 갯수(중복제거)
	$query_str_arr[4]="SELECT count(distinct referer) FROM homepagelog WHERE (referer LIKE '%http://www.3030eng.com%')";
	//referer 필드에서 총 데이터의 갯수(중복제거)
	$query_str_arr[5]="SELECT count(distinct referer) FROM homepagelog";

	//년도만 선택된 경우
	if($year!='no' && $month=='no'){
		for($i=0; $i<count($query_str_arr); $i++){
			if($i==5){
				$query_str_arr[$i]=$query_str_arr[$i]. " WHERE left(postdate,4)='".$year."'";
				continue;
			}
			$query_str_arr[$i]=$query_str_arr[$i]. " AND (left(postdate,4)='".$year."')";
		}
	}

	//년도, 월 모두 선택된 경우
	if($year!='no' && $month!='no'){
		for($i=0; $i<count($query_str_arr); $i++){
			if($i==5){
				$query_str_arr[$i]=$query_str_arr[$i]. " WHERE left(postdate,7)='".$year."-".$month."'";
				continue;
			}
			$query_str_arr[$i]=$query_str_arr[$i]. " AND (left(postdate,7)='".$year."-".$month."')";
		}
	}


	for($i=0; $i<count($path_arr); $i++){
		$result = mysqli_query($link, $query_str_arr[$i]);
		mysqli_commit($link);
		$row = mysqli_fetch_assoc($result);
		$path_arr[$i] = (int)$row['count(distinct referer)'];
	}
	/*key값 없이 JSON데이터에 담는 방법(user_for_etc.js에서 바로 인덱스 접근해서 테이블에 뿌려주기에 용이하다~)*/
	$arr = array();
	for($i=0; $i<count($path_arr);$i++){
		$arr[$i] = $path_arr[$i];
	}
	echo '{"path": ' . json_encode($arr) . '}';
}

function getEtcUser1($year, $month){
	

	// mysqli 트랜잭션
	$link = mysqli_connect("www.3030eng.com","root","Rakgk5go!~","home3030");
	if(mysqli_connect_errno()){
		printf("Connect failed: %s",mysqli_connect_errno());
		exit();
	}
	mysqli_begin_transaction($link);

	//년도, 월 아무것도 선택하지 않은 경우(전체데이터)
	if($year=='no'&&$month=='no'){
		$result = mysqli_query($link, "SELECT DISTINCT referer FROM homepagelog");
	}
	//년도만 선택한 경우
	else if($year!='no'&&$month=='no'){
		$result = mysqli_query($link, "SELECT DISTINCT referer FROM homepagelog WHERE left(postdate,4)=".$year);
	}
	// 년도, 월 모두 선택한 경우
	else if(1<=$month&&$month<10){
		$month = "0".(string)$month;
		$result = mysqli_query($link, "SELECT DISTINCT referer FROM homepagelog WHERE left(postdate,7)='".$year."-".$month."'");
	}
	mysqli_commit($link);

	//각 변수들 선언 및 카운팅
	$search_naver_cnt=0; $search_daum_cnt=0; $search_google_cnt=0;
	$cafe_naver_cnt=0; $cafe_daum_cnt=0;
	$blog_naver_cnt=0; $blog_daum_cnt=0;
	$mail_naver_cnt=0; $mail_daum_cnt=0; $mail_googl_cnt=0;
	$direct_cnt=0;
	$etc_cnt=0;
	$total_cnt=0;
	while($row = mysqli_fetch_assoc($result)){
		if(strpos($row['referer'], 'search.naver')){
			$search_naver_cnt++;
		}
		else if(strpos($row['referer'], 'search.daum')){
			$search_daum_cnt++;
		}
		else if(strpos($row['referer'], 'google')){
			$search_google_cnt++;	
		}

		else if(strpos($row['referer'], 'cafe.naver')){
			$cafe_naver_cnt++;
		}
		else if(strpos($row['referer'], 'map.daum')){
			$cafe_daum_cnt++;
		}

		else if(strpos($row['referer'], 'blog.naver')){
			$blog_naver_cnt++;
		}
		else if(strpos($row['referer'], 'blog.daum')){
			$blog_daum_cnt++;
		}

		else if(strpos($row['referer'], 'mail.naver')){
			$mail_naver_cnt++;
		}
		else if(strpos($row['referer'], 'mail2.daum')){
			$mail_daum_cnt++;
		}
		else if(strpos($row['referer'], 'mail.google')){
			$mail_googl_cnt++;
		}

		else if(strpos($row['referer'], 'http://www.3030eng.com')){
			$direct_cnt++;
		}
		// else if(strpos($row['referer'], '')){
		// 	$etc_cnt++;
		// }
			$total_cnt++;
	}
	/*key값 없이 JSON데이터에 담는 방법(user_for_etc.js에서 바로 인덱스 접근해서 테이블에 뿌려주기에 용이하다~)*/
	$arr = array($search_naver_cnt, $search_daum_cnt, $search_google_cnt,
				 $cafe_naver_cnt, $cafe_daum_cnt,
				 $blog_naver_cnt, $blog_daum_cnt,
				 $mail_naver_cnt, $mail_daum_cnt, $mail_googl_cnt,
				 $direct_cnt, $etc_cnt,  $total_cnt);
	echo '{"etc": ' . json_encode($arr) . '}';
}

//포털별 검색어 데이터 get
function getEtcUser2($path_type, $portal_type, $year, $month) {

	// mysqli 트랜잭션
	$link = mysqli_connect("www.3030eng.com","root","Rakgk5go!~","home3030");
	mysqli_set_charset($link, 'utf8');
	if(mysqli_connect_errno()){
		printf("Connect failed: %s",mysqli_connect_errno());
		exit();
	}
	mysqli_begin_transaction($link);

	// 클라이언트에서 "검색"인 경우
	if($path_type=='search'){
		$search_word="";
		$parsing_front="";
		$parsing_back="";
		if($portal_type=="naver"){
			$search_word="search.naver";
			$parsing_front="query=";
			$parsing_back="&";//뒤에 없는 경우도 있다..
		}
		else if($portal_type=="daum"){
			$search_word="search.daum";
			$parsing_front="&q=";
			$parsing_back="&";//뒤에 없는 경우도 있다..
		}
		else if($portal_type=="google"){
			$search_word="google";
			$parsing_front="&q=";
			$parsing_back="&";
		}
		

		//검색어(중복refer허용, 그래야 검색어 카운트가 되니까)
		//search.포탈명 포함 and 첫번째 파싱문자열 포함하는 데이터 가져오는 쿼리문(첫번째 파싱문자열 포함하지 않는 것들은 url에 키워드가 존재하지 않아 의미가 없다..)
		//년도, 월 아무것도 선택하지 않은 경우(전체데이터)
		if($year=='no'&&$month=='no'){
			$result = mysqli_query($link, "SELECT SUBSTRING_INDEX(referer,'".$parsing_front."',-1) FROM homepagelog WHERE (referer LIKE '%".$search_word."%' AND referer LIKE '%".$parsing_front."%')");
		}
		//년도만 선택한 경우
		else if($year!='no'&&$month=='no'){
			$result = mysqli_query($link, "SELECT SUBSTRING_INDEX(referer,'".$parsing_front."',-1) FROM homepagelog WHERE (referer LIKE '%".$search_word."%' AND referer LIKE '%".$parsing_front."%') AND left(postdate,4)=".$year);
		}
		// 년도, 월 모두 선택한 경우
		else if(1<=$month&&$month<10){
			$month = "0".(string)$month;
			$result = mysqli_query($link, "SELECT SUBSTRING_INDEX(referer,'".$parsing_front."',-1) FROM homepagelog WHERE (referer LIKE '%".$search_word."%' AND referer LIKE '%".$parsing_front."%') AND left(postdate,7)='".$year."-".$month."'");
		}
		mysqli_commit($link);


		$search_keyword_arr=array(); //$keyword_arr["aa"]="AA";
		while($row = mysqli_fetch_assoc($result)){
			// 종료파싱키워드로잘라내는코드(어레이로나온다..그래서아래줄에서따로 골라담아줌)
			$substr_arr = explode($parsing_back,urldecode($row["SUBSTRING_INDEX(referer,'".$parsing_front."',-1)"]));
			// $substr_arr = explode($parsing_back,urldecode(str_replace('+',' ',$row["SUBSTRING_INDEX(referer,'".$parsing_front."',-1)"])));
			$keyword = $substr_arr[0]; //띄어쓰기 있으면 다른 키워드로 간주..
			// $keyword = preg_replace("/\s+/", "", $substr_arr[0]); //띄어쓰기 있어도 같은 키워드로 간주..(띄어쓰기 모두 제거해버리는 것)

			// echo $keyword."@@@@@";
			if($keyword==""){
				continue;
			}
			else if(json_encode($keyword)!='') {
				if(array_key_exists($keyword, $search_keyword_arr)==false){//존재하지 않으면 키워드 추가
					$search_keyword_arr[$keyword]=1;
				}
				else{//$keyword_arr에 존재하면 ++..
					$search_keyword_arr[$keyword]++;
				}
			}
		}
		arsort($search_keyword_arr);

		// 클라이언트 단에서 JSON객체 사용하기 어려워서.. 임시방편으로 보내기용 arr생성 후 사용..
		$send_keyword_arr=[];
		$i=0;
		foreach($search_keyword_arr as $key=>$value){
			$send_keyword_arr[$i++]=$key.":::".$value;
		}
	}

	// 클라이언트에서 "카페"인 경우
	else if($path_type=='cafe'){
		if($portal_type=="naver"){
			$search_word="cafe.naver";
			$parsing_front="&";
			// $parsing_back="&";//뒤에 없는 경우도 있다..
		}
		else if($portal_type=="daum"){
			$search_word="map.daum";
			$parsing_front="&";
			// $parsing_back="&";//뒤에 없는 경우도 있다..
		}

		//검색어(중복refer허용, 그래야 검색어 카운트가 되니까)
		//년도, 월 아무것도 선택하지 않은 경우(전체데이터)
		if($year=='no'&&$month=='no'){
			$result = mysqli_query($link, "SELECT SUBSTRING_INDEX(referer,'".$parsing_front."',1) FROM homepagelog WHERE referer LIKE '%".$search_word."%'");
		}
		//년도만 선택한 경우
		else if($year!='no'&&$month=='no'){
			$result = mysqli_query($link, "SELECT SUBSTRING_INDEX(referer,'".$parsing_front."',1) FROM homepagelog WHERE (referer LIKE '%".$search_word."%') AND left(postdate,4)=".$year);
		}
		// 년도, 월 모두 선택한 경우
		else if(1<=$month&&$month<10){
			$month = "0".(string)$month;
			$result = mysqli_query($link, "SELECT SUBSTRING_INDEX(referer,'".$parsing_front."',1) FROM homepagelog WHERE (referer LIKE '%".$search_word."%') AND left(postdate,7)='".$year."-".$month."'");
		}
		mysqli_commit($link);

		$cafe_keyword_arr=array(); //$keyword_arr["aa"]="AA";
		while($row = mysqli_fetch_assoc($result)){
			$keyword = $row["SUBSTRING_INDEX(referer,'".$parsing_front."',1)"];

			if($keyword==""){
				continue;
			}
			else if(json_encode($keyword)!='') {
				if(array_key_exists($keyword, $cafe_keyword_arr)==false){//존재하지 않으면 키워드 추가
					$cafe_keyword_arr[$keyword]=1;
				}
				else{//$keyword_arr에 존재하면 ++..
					$cafe_keyword_arr[$keyword]++;
				}
			}
		}
		arsort($cafe_keyword_arr);

		// 클라이언트 단에서 JSON객체 사용하기 어려워서.. 임시방편으로 보내기용 arr생성 후 사용..
		$send_keyword_arr=[];
		$i=0;
		foreach($cafe_keyword_arr as $key=>$value){
			$send_keyword_arr[$i++]=$key.":::".$value;
		}
	}

	// 클라이언트에서 "블로그"인 경우
	else if($path_type=='blog'){
		if($portal_type=="naver"){
			$search_word="blog.naver";
			// $parsing_front="&";
			// $parsing_back="&";//뒤에 없는 경우도 있다..
		}
		else if($portal_type=="daum"){
			$search_word="blog.daum";
			// $parsing_front="&";
			// $parsing_back="&";//뒤에 없는 경우도 있다..
		}

		//검색어(중복refer허용, 그래야 검색어 카운트가 되니까)
		//년도, 월 아무것도 선택하지 않은 경우(전체데이터)
		if($year=='no'&&$month=='no'){
			$result = mysqli_query($link, "SELECT referer FROM homepagelog WHERE referer LIKE '%".$search_word."%'");
		}
		//년도만 선택한 경우
		else if($year!='no'&&$month=='no'){
			$result = mysqli_query($link, "SELECT referer FROM homepagelog WHERE (referer LIKE '%".$search_word."%') AND left(postdate,4)=".$year);
		}
		// 년도, 월 모두 선택한 경우
		else if(1<=$month&&$month<10){
			$month = "0".(string)$month;
			$result = mysqli_query($link, "SELECT referer FROM homepagelog WHERE (referer LIKE '%".$search_word."%') AND left(postdate,7)='".$year."-".$month."'");
		}
		mysqli_commit($link);

		$blog_keyword_arr=array(); //$keyword_arr["aa"]="AA";
		while($row = mysqli_fetch_assoc($result)){
			$keyword = $row["referer"];

			if($keyword==""){
				continue;
			}
			else if(json_encode($keyword)!='') {
				if(array_key_exists($keyword, $blog_keyword_arr)==false){//존재하지 않으면 키워드 추가
					$blog_keyword_arr[$keyword]=1;
				}
				else{//$keyword_arr에 존재하면 ++..
					$blog_keyword_arr[$keyword]++;
				}
			}
		}
		arsort($blog_keyword_arr);

		// 클라이언트 단에서 JSON객체 사용하기 어려워서.. 임시방편으로 보내기용 arr생성 후 사용..
		$send_keyword_arr=[];
		$i=0;
		foreach($blog_keyword_arr as $key=>$value){
			$send_keyword_arr[$i++]=$key.":::".$value;
		}
	}

	echo '{"etc_keyword": ' . json_encode($send_keyword_arr) . '}';
	// echo '{"etc_keyword": ' . "[2467,136,38,138,55,129,1,1,1,0,0,0,3255]" . '}';
}

////아직구현안함//////
function getWines(){
	// global $global_year, $global_month;
	$sql = "select * FROM wine ORDER BY name";

	echo "$app->get('/wines', 'getWines');";
}

function getWine($id){
	// global $global_year, $global_month;
	echo "$app->get('/wines/:",$id,", 'getWine');";
}

function findByName($query){
	// global $global_year, $global_month;
	echo "$app->get('/wines/search/:query', 'findByName');";
}

function addWine(){
	// global $global_year, $global_month;
	echo "$app->post('/wines', 'addWine');";
}

function updateWine($id){
	// global $global_year, $global_month;
	echo "$app->put('/wines/:id','updateWine');";
}

function deleteWine($id){
	// global $global_year, $global_month;
	echo "$app->delete('/wines/:id', 'deleteWine');";
}
?>