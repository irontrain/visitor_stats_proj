<?php 
	session_start();

	// $member_id = "root";
	// $member_password = "Rakgk5go!~";
	$member_id = "ironman";
	$member_password = "3030";

	//아이디 없는 경우
	if(empty($_POST["member_id"])){
		echo "<meta http-equiv=\'Content-Type\' content=\'text/html; charset=utf-8\' />
			  <script type='text/javascript'>alert('아이디를 입력하세요');
			  location.replace('/user_login.html');</script>";
	}
	//비밀번호 없는 경우
	else if(empty($_POST["member_password"])){
		echo "<meta http-equiv=\'Content-Type\' content=\'text/html; charset=utf-8\' />
			  <script type='text/javascript'>alert('비밀번호를 입력하세요');
			  location.replace('/user_login.html');</script>";
	}
	else{
		//아이디 잘못된 경우
		if(strcmp($_POST["member_id"], $member_id) != 0){
			echo "<meta http-equiv=\'Content-Type\' content=\'text/html; charset=utf-8\' />
				  <script type='text/javascript'>alert('아이디가 일치하지 않습니다');
			  	  location.replace('/user_login.html');</script>";
		}
		//비밀번호 잘못된 경우
		else if (strcmp($_POST["member_password"], $member_password) != 0){
			echo "<meta http-equiv=\'Content-Type\' content=\'text/html; charset=utf-8\' />
				  <script type='text/javascript'>alert('비밀번호가 일치하지 않습니다');
			  	  location.replace('/user_login.html');</script>";
		}
		//로그인 성공 경우
		else{
			$_SESSION["member_id"] = $_POST["member_id"];
			$_SESSION["member_password"] = $_POST["member_password"];
			echo "<meta http-equiv=\'Content-Type\' content=\'text/html; charset=utf-8\' />
				  <script type='text/javascript'>
			  	  location.replace('/user_index.php');</script>";
		}
	}
?>