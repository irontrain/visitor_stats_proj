<?php
	session_start();
	//세션에 로그인 정보가 없는 경우..
	if(!isset($_SESSION["member_id"]) || !isset($_SESSION["member_password"])){
		echo "<meta http-equiv=\'Content-Type\' content=\'text/html; charset=utf-8\' />
			  <script>
				alert('로그인 되지 않았습니다, 다시 시도하여 주십시오..');
			  	location.replace('/user_login.html');
			  </script>";
	}
	//세션에 로그인 정보가 있는 경우
	else{
		session_destroy();
		echo "<meta http-equiv=\'Content-Type\' content=\'text/html; charset=utf-8\' />
			  <script>
				alert('로그아웃 되었습니다 ".$_SESSION["member_id"]."님');
			  	location.replace('/user_login.html');
			  </script>";
	}
?>
