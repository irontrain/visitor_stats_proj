<!-- 원본 /user_index.html 소스코드 -->
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
		<title>Statistics html</title>
		<link href="http://fonts.googleapis.com/earlyaccess/nanumgothic.css" rel="stylesheet"/>
		<script src="http://code.jquery.com/jquery-1.8.2.min.js"></script>
		<!--초기화-->
		<style> 
			*{margin:0; padding: 0; /*border:solid; border-width:1px;*/}
			body {font-family: 'Arial', sans-serif;}
			li{ list-style: none;}
			a {text-decoration: none;}
			img {border: 0;}
		</style>





<?php
	session_start();

	//세션에 로그인 정보가 없는 경우..
	if(!isset($_SESSION["member_id"]) || !isset($_SESSION["member_password"])){
		?>
		<script type='text/javascript'>alert('로그인 되지 않았습니다, 로그인하여 주십시오..');
			 	location.replace('/user_login.html');</script>;
		<?php
	}
	//세션에 로그인 정보가 있는 경우
	else{
		?>
		










		
		<!--헤더-->
		<style>
		#main_header{
			width:960px;
			margin:0 auto;

			height:180px;
			position:relative;
		}
		#main_header > #title{
			/*position: absolute;
			left: 20px; top: 30px;*/
			width: 130px;
			padding-top: 15px;
			margin:0 auto;
		}
		#main_header > #main_gnb{
			position: absolute;
			right: 0; top: 0;
		}
		#main_header > #main_lnb{
			position: absolute;
			right: 0; bottom: 10px;
		}
		</style>
		<!--타이틀-->
		<style>
		#title{
			font-family: 'Arial', sans-serif;
			color: #5D5D5D;
		}
		</style>
		<!--메뉴1-->
		<!--
		<style>
			#main_gnb > ul {overflow: hidden;}
			#main_gnb > ul >li{float: left;}
			#main_gnb>ul>li>a{
				display: block;
				padding: 2px 10px;
				border: 1px solid #5D5D5D;
				color: #BDBDBD;
			}
			#main_gnb>ul>li>a:hover{
				background: black;
				color: white;
			}
			#main_gnb>ul>li:first-child>a{
				border-radius: 10px 0 0 10px;
			}
			#main_gnb>ul>li:last-child>a{
				border-radius: 0 10px 10px 0;
			}
		</style>
		-->
		<!-- 선택박스 CSS부분-->
    	<style>
        #styled_select{
        	overflow: hidden;
            float:left;
           margin-top:22px;
           margin-right: 100px;
           padding-top: 20px;
           /*margin-left: 50px;*/
        }
        #styled_select select {
           background: #ED0000;
           /*width: 120px;*/
           /*padding: 5px;*/
           font-size: 14px;
           color:white;
           line-height: 1;
           border: 0;
           border-radius: 0;
           height: 20px;
        }
        #styled_select #year_select{
			width:70px;
        }
        #styled_select #month_select{
			width:50px;
        }
        #styled_select label {
            position: relative;
            font-size: 14px;
            font-weight: bold;
            color: #353535;
            text-align: center;
            top: 2px;
            left: 7px;
            letter-spacing: 2px;
        }
    	</style>
		<!--메뉴2-->
		<style>
		#main_lnb > ul {overflow: hidden;}
		#main_lnb > ul >li{float: left;}
		#main_lnb>ul>li>a{
			/*display: block;*/
			padding: 20px 30px;
			border: 1px solid #ED0000;
			color: #ED0000;
			font-family: 'Arial', sans-serif;
			font-weight: bold;
			font-size:16px;
		}
		#main_lnb>ul>li>a:hover{
			background: #ED0000;
			color: white;
		}
		#main_lnb>ul>li:first-child>a{
			border-radius: 10px 0 0 10px;
		}
		#main_lnb>ul>li:last-child>a{
			border-radius: 0 10px 10px 0;
		}
		</style>
		<!--컨텐트-->
		<style>
		#content{
			width: 960px; margin: 0 auto;
			overflow: hidden;
			padding-top: 35px;

		}
		#content > #main_section{
			width:957px;
			float: left;
			border: 1px black;
			font-family: 'Arial', sans-serif;
		}
		#content > #main_aside{
			width: 200px;
			float: right;
			border: 1px solid black;
		}
		</style>
		<!--본문 article-->
		<style>
		#main_section>article.main_article{
			margin-bottom: 10px;
			padding: 20px;
			border: 1px solid black;

		}
		</style>
		<!--본문 aside-->
			<!--
			<style>
			</style>
		-->
		<!--로그아웃 버튼-->
		<style>
		#logout_button{
			/*버튼위치 기존설정*/
			position: absolute;
			right: 0; bottom: 120px;
			/*부트스트랩 내 .btn 내용*/
			display: inline-block;
			padding: 2px 5px;
			margin-bottom: 0;
			font-size: 12px;
			font-weight: 400;
			line-height: 1.42857143;
			text-align: center;
			white-space: nowrap;
			vertical-align: middle;
			/*-ms-touch-action: manipulation;*/
			/*touch-action: manipulation;*/
			cursor: pointer;
			/*-webkit-user-select: none;*/
			/*-moz-user-select: none;*/
			/*-ms-user-select: none;*/
			/*user-select: none;*/
			background-image: none;
			border: 1px solid transparent;
			border-radius: 4px;
			/*부트스트랩 내 .btn .btn-primary 내용들*/
			color: #fff;
			background-color: #337ab7; /*#8BBCFF;*/ 
			border-color: #2e6da4; /*#79AAFF;*/ 
		}
		</style>
		<style></style>
		<style></style>
</head>
<body>
	<header id="main_header">
		<hgroup id="title" >
			<a href="user_index.php"><img src="user_img/3030logo.gif" style="width: 130px; height:auto;"/>
				<h3 style="float:right; color:#ED0000;">Visitor Stats</h3></a>
			</hgroup>
			<button id="logout_button" onClick="logout()">로그아웃</button>
			<!-- 		
			<nav id="main_gnb">
				<ul>
					<li><a href="#">Web</a></li>
					<li><a href="#">Mobile</a></li>
					<li><a href="#">Game</a></li>
					<li><a href="#">Simulation</a></li>
					<li><a href="#">Data</a></li>
				</ul>
			</nav> 
		-->
		<div id="styled_select">
        <form name="year_month_form">
            <select class="target" id="year_select" name="year_select_name">
                <option>년도</option>
                <option>2013</option>
                <option>2014</option>
                <option>2015</option>
                <option selected>2016</option>
            </select>
            <select class="target" id="month_select" name="month_select_name">
                <option>월</option>
                <option >1</option>
                <option >2</option>
                <option selected>3</option>
                <option >4</option>
                <option >5</option>
                <option >6</option>
                <option >7</option>
                <option >8</option>
                <option >9</option>
                <option >10</option>
                <option >11</option>
                <option >12</option>
            </select>
        </form>
    	</div>
		<nav id="main_lnb">
			<ul id="main_ul_id">
				<li><a href="#" id="month_id">방문자별</a></li>
				<li><a href="#" id="portal_id">포털별</a></li>
				<li><a href="#" id="path_id">경로별</a></li>
				<li><a href="#" id="etc_id">기타</a></li>
			</ul>
		</nav>
	</header>
	<div id="content">
		<h2 style="color:#5D5D5D; font-family: 'Arial', sans-serif; margin-top:40px; "> </h2>
		<div id="main_section">
				<!-- <article class="main_article"><h1>Main Article</h1>
				<p>Main Article paragraph contents</p>
				</article>
				<article class="main_article"><h1>Main Article</h1>
				<p>Main Article paragraph contents</p>
				</article>
				<article class="main_article"><h1>Main Article</h1>
				<p>Main Article paragraph contents</p>
			</article> -->
				<!--
				<?php
				//if( empty($_GET['id']) == false ) {
				//	include_once "index_stats/".$_GET['id'].".php";
	   	    	//echo file_get_contents("index_stats/".$_GET['id'].".php");
	   			//}
	    		?>
	    	-->
	    </div>
			<!-- 		
			<aside id="main_aside">
				<h1>Main Aside</h1>
				<p>Main Aside paragraph contents</p>
			</aside> 
		-->
	</div>
	<script type="text/javascript" src="user_js/user_list.js"charset="utf-8">
	</script>
</body>
</html>


	<?php
	}
?>
