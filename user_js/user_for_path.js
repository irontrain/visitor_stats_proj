    var rootURL = "";   

    var doughnut_type=0; //명수or비율 선택 시 list의 값들 재활용하기 위해 전역으로 선언(0이면 비율, 1이면 명수)
    var tooltip_type=""; //tooltip 문자열도 "%"일때와, "명"일때 다르게 해줌

    var myDoughnutChart; 

    var add_url_string="";

    //페이지 최초 진입시
    // alert("최초");
    if($('#myChart2').length){
      // $("#ready_state_id").hide();
      // $("#show_state_id").show();
      //아무것도 선택하지 않은 경우
    if($('#year_select').val()=='년도' && $('#month_select').val()=='월'){
      //서버에서 전체데이터 받아오기
      add_url_string="/"+"no"+"/"+"no";
      $("#before_select_year").hide();
      $("#after_select_year").show();
      get_pathData();
    }
    else if($('#year_select').val()=="년도" && $('#month_select').val()!="월"){
      //년 없이 월만 선택한 경우
      $("#before_select_year").show();
      $("#after_select_year").hide();
    }
    else if($('#year_select').val()!="년도" && $('#month_select').val()=="월"){
      //년도만 선택한 경우
      add_url_string="/"+$('#year_select').val()+"/"+"no";
      get_pathData();
      $("#before_select_year").hide();
      $("#after_select_year").show();
    }
    else if($('#year_select').val()!="년도" && $('#month_select').val()!="월"){
      //년도와 월 모두 선택한 경우
      add_url_string="/"+$('#year_select').val()+"/"+$('#month_select').val();
      get_pathData();
      $("#before_select_year").hide();
      $("#after_select_year").show();
    }
    }

    //날짜 및 월 선택 했을 때
    var id=0;
    document.getElementById('year_select').onchange = function(){y_m_change()};
    document.getElementById('month_select').onchange = function(){y_m_change()};
    function y_m_change(){
    if($('#myChart2').length){
      myDoughnutChart.destroy();
      $("#ready_state_id").show();
      $("#show_state_id").hide();
      // alert("경로"+id++);
      //아무것도 선택하지 않은 경우
    if($('#year_select').val()=='년도' && $('#month_select').val()=='월'){
      //서버에서 전체데이터 받아오기
      add_url_string="/"+"no"+"/"+"no";
      $("#before_select_year").hide();
      $("#after_select_year").show();
      get_pathData();
    }
    else if($('#year_select').val()=="년도" && $('#month_select').val()!="월"){
      //년 없이 월만 선택한 경우
      $("#before_select_year").show();
      $("#after_select_year").hide();
    }
    else if($('#year_select').val()!="년도" && $('#month_select').val()=="월"){
      //년도만 선택한 경우
      add_url_string="/"+$('#year_select').val()+"/"+"no";
      get_pathData();
      $("#before_select_year").hide();
      $("#after_select_year").show();
    }
    else if($('#year_select').val()!="년도" && $('#month_select').val()!="월"){
      //년도와 월 모두 선택한 경우
      add_url_string="/"+$('#year_select').val()+"/"+$('#month_select').val();
      get_pathData();
      $("#before_select_year").hide();
      $("#after_select_year").show();
    }
    }
    }

    function get_pathData(){
    $.ajax({
      type: 'GET',
      url: rootURL+'/path'+add_url_string, 
      dataType: "json",
      success: gen_pathDoughnut_chart
      });
    }

    function gen_pathDoughnut_chart(data){
      var list = data == null ? [] : (data.path instanceof Array ? data.path : [data.path]);

      if(doughnut_type==0){
        // 테이블 채워주는 부분
        var td_id_arr = ["value_search","value_cafe","value_blog","value_mail","value_direct"];
        for(var i=0; i<td_id_arr.length; i++){
          document.getElementById(td_id_arr[i]).innerHTML = Math.floor((list[i]/list[5])*100)+" %";
        }

        var data = [
        {
          value: Math.floor((list[0]/list[5])*100),
          color:"#6799FF",
          highlight: "#B2CCFF",
          label: "검색"
        },
        {
          value: Math.floor((list[1]/list[5])*100),
          color: "#F29661",
          highlight: "#FFC19E",
          label: "카페"
        },
        {
          value: Math.floor((list[2]/list[5])*100),
          color: "#BCE55C",
          highlight: "#CEF279",
          label: "블로그"
        },
        {
          value: Math.floor((list[3]/list[5])*100),
          color: "#A566FF",
          highlight: "#D1B2FF",
          label: "메일"
        },
        {
          value: Math.floor((list[4]/list[5])*100),
          color: "#A6A6A6",
          highlight: "#D5D5D5",
          label: "직접접속"
        },
        ];
        tooltip_type="<%= label %> : <%= value %>%";
      }
      else if(doughnut_type==1){
        // 테이블 채워주는 부분
        var td_id_arr = ["value_search","value_cafe","value_blog","value_mail","value_direct"];
        for(var i=0; i<td_id_arr.length; i++){
          document.getElementById(td_id_arr[i]).innerHTML = list[i]+" 명";
        }

        var data = [
        {
          value: list[0],
          color:"#6799FF",
          highlight: "#B2CCFF",
          label: "검색"
        },
        {
          value: list[1],
          color: "#F29661",
          highlight: "#FFC19E",
          label: "카페"
        },
        {
          value: list[2],
          color: "#BCE55C",
          highlight: "#CEF279",
          label: "블로그"
        },
        {
          value: list[3],
          color: "#A566FF",
          highlight: "#D1B2FF",
          label: "메일"
        },
        {
          value: list[4],
          color: "#A6A6A6",
          highlight: "#D5D5D5",
          label: "직접접속"
        },
        ];
        tooltip_type="<%= label %> : <%= value %>명";
      }

      var ctx = document.getElementById("myChart2").getContext("2d");
      myDoughnutChart = new Chart(ctx).Doughnut(data, {
        // animateScale: true
        animationSteps:60, //애니메이션되는 속도, 낮을 수록 빠름
        // showTooltips:true, //tooltip기능 ON, OFF
        //tooltipFillColor: "green", // tooltip배경색깔
        
        /*tooltip자동으로 보이게하는 옵션 설정*/
        // tooltipEvents:[],
        // showTooltips:true,
        // onAnimationComplete: function(){
        //   this.showTooltip(this.segments, true);
        // },
        tooltipTemplate: tooltip_type
      });
      $("#ready_state_id").hide();
      $("#show_state_id").show();
    }

    // select box에서 명수/비율 선택시
    function updateChart(frm) {
      var selected_type = frm.type_select.value;

      if(selected_type=="비율"){
        //비율 선택한 경우
        doughnut_type=0;
      }
      else if(selected_type=="명수"){
        //명수 선택한 경우
        doughnut_type=1;
      }
      myDoughnutChart.destroy();

      //아무것도 선택하지 않은 경우
      if($('#year_select').val()=='년도' && $('#month_select').val()=='월'){
      //서버에서 전체데이터 받아오기
      get_pathData();
      }
      else if($('#year_select').val()=="년도" && $('#month_select').val()!="월"){
      //년 없이 월만 선택한 경우
      $("#before_select_year").show();
      $("#after_select_year").hide();
      }
      else if($('#year_select').val()!="년도" && $('#month_select').val()=="월"){
      //년도만 선택한 경우
      // add_url_string="/"+$('#year_select').val();
      $("#before_select_year").hide();
      $("#after_select_year").show();
      get_pathData();
      }
      else if($('#year_select').val()!="년도" && $('#month_select').val()!="월"){
      //년도와 월 모두 선택한 경우
      // add_url_string="/"+$('#year_select').val()+"/"+$('#month_select').val();
      $("#before_select_year").hide();
      $("#after_select_year").show();
      get_pathData();
      }
      }

      //차트 클릭이벤트 리스너
      $("#myChart2").click(
        function(evt){
          $("#main_section").load("user_index_stats/etc.html");
        }
      );