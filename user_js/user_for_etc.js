
  	var rootURL = "";   

  	var portal_type="";
    var path_type="";

    var add_url_string="";

    //페이지 최초 진입시
    // alert("최초");
    if($('#table_id').length){
      // $("#ready_state_id").hide();
      // $("#show_state_id").show();
      //아무것도 선택하지 않은 경우
    if($('#year_select').val()=='년도' && $('#month_select').val()=='월'){
      //서버에서 전체데이터 받아오기
      add_url_string="/"+"no"+"/"+"no";
      $("#before_select_year").hide();
      $("#after_select_year").show();
      fill_table();
    }
    else if($('#year_select').val()=="년도" && $('#month_select').val()!="월"){
      //년 없이 월만 선택한 경우
      $("#before_select_year").show();
      $("#after_select_year").hide();
    }
    else if($('#year_select').val()!="년도" && $('#month_select').val()=="월"){
      //년도만 선택한 경우
      add_url_string="/"+$('#year_select').val()+"/"+"no";
      fill_table();
      $("#before_select_year").hide();
      $("#after_select_year").show();
    }
    else if($('#year_select').val()!="년도" && $('#month_select').val()!="월"){
      //년도와 월 모두 선택한 경우
      add_url_string="/"+$('#year_select').val()+"/"+$('#month_select').val();
      fill_table();
      $("#before_select_year").hide();
      $("#after_select_year").show();
    }
    }

    //날짜 및 월 선택 했을 때
    var id=0;
    document.getElementById('year_select').onchange = function(){y_m_change()};
    document.getElementById('month_select').onchange = function(){y_m_change()};
    function y_m_change(){
    if($('#table_id').length){
      $("#ready_state_id").show();
      $("#show_state_id").hide();
      $('#keyword_table_id tr').remove();
      //아무것도 선택하지 않은 경우
    if($('#year_select').val()=='년도' && $('#month_select').val()=='월'){
      //서버에서 전체데이터 받아오기
      add_url_string="/"+"no"+"/"+"no";
      $("#before_select_year").hide();
      $("#after_select_year").show();
      fill_table();
      $('#keyword_div').hide();
      // var keyword_table = document.getElementById('keyword_div');
      // keyword_table.innerHTML="";
    }
    else if($('#year_select').val()=="년도" && $('#month_select').val()!="월"){
      //년 없이 월만 선택한 경우
      $("#before_select_year").show();
      $("#after_select_year").hide();
    }
    else if($('#year_select').val()!="년도" && $('#month_select').val()=="월"){
      //년도만 선택한 경우
      add_url_string="/"+$('#year_select').val()+"/"+"no";
      // glob_year = $('#year_select').val();
      $("#before_select_year").hide();
      $("#after_select_year").show();
      fill_table();
      $('#keyword_div').hide();
      // var keyword_table = document.getElementById('keyword_div');
      // keyword_table.innerHTML="";
    }
    else if($('#year_select').val()!="년도" && $('#month_select').val()!="월"){
      //년도와 월 모두 선택한 경우
      add_url_string="/"+$('#year_select').val()+"/"+$('#month_select').val();
      $("#before_select_year").hide();
      $("#after_select_year").show();
      fill_table();
      $('#keyword_div').hide();
      // var keyword_table = document.getElementById('keyword_div');
      // keyword_table.innerHTML="";
    }
    }
    }

	function fill_table(){
		$.ajax({
			type: 'GET',
      		url: rootURL+'/etc'+add_url_string, 
			dataType: "json",
			success: fill_table_content
		});
	}

	function get_detail_data(){
		$.ajax({
			type: 'GET',
			url: rootURL+'/etc2'+path_type+portal_type+add_url_string,
			dataType: "json",
			success: fill_detail_table
		});
	}

	function fill_detail_table(data){
    
		var list = data == null ? [] : (data.etc_keyword instanceof Array ? data.etc_keyword : [data.etc_keyword]);
		// for(var i=0; i<list.length; i++){
		// 	document.write(list[i]+'</br>');
		// }
		//테이블 채우는 부분
		$("#keyword_div").css({ 'display': "inline-block" });
		var keyword_table = document.getElementById('keyword_table_id');
      	while (keyword_table.hasChildNodes()) {
	        keyword_table.removeChild(keyword_table.lastChild);
      	}

        if(list.length==0){
          var tr_element = document.createElement('tr');
          var td_count = document.createElement('td');
          var td_label = document.createElement('td');
          var td_value = document.createElement('td');
          td_count.innerHTML='정보없음';
          td_label.innerHTML='정보없음';
          td_value.innerHTML='정보없음';
          tr_element.appendChild(td_count);
          tr_element.appendChild(td_label);
          tr_element.appendChild(td_value);
          keyword_table.appendChild(tr_element);
        }
        else{
          for(var i=0; i<list.length; i++){
          // 3030영어:::427 -> 구분자 ':::'로 분리..
          var temp_list = list[i].split(":::");

          var tr_element = document.createElement('tr');
          var td_count = document.createElement('td');
          var td_label = document.createElement('td');
          var td_value = document.createElement('td');
          if(path_type=='/cafe'||path_type=='/blog'){
            var aTag = document.createElement('a');
            aTag.setAttribute('href', temp_list[0]);
            aTag.setAttribute('target', '_blank');
            aTag.innerHTML=temp_list[0];
            td_label.appendChild(aTag);
          }
          else{
            td_label.innerHTML=temp_list[0];
          }
          td_count.innerHTML=(i+1)+". ";
          td_value.innerHTML=temp_list[1];
          tr_element.appendChild(td_count);
          tr_element.appendChild(td_label);
          tr_element.appendChild(td_value);
          keyword_table.appendChild(tr_element);
          }
        }
		    
        var keyword_div_height=10;
  			keyword_div_height=$("#table_id").height();
			   $("#keyword_div").height(keyword_div_height-24);//-24는 div내 padding값+border값 만큼 빼줘서 table_id와 사이즈 맞춰주는 것, -100은 그냥 보기 좋으라고
	}

	function fill_table_content(data){
		var td_id_arr = ["search_naver","search_daum","search_google",
						 "cafe_naver","cafe_daum",
						 "blog_naver","blog_daum",
						 "mail_naver","mail_daum","mail_google",
						 "direct","etc","total"];

		var list = data == null ? [] : (data.etc instanceof Array ? data.etc : [data.etc]);
		$.each(list, function(index, value){
			//document.write(index + ":" + value+"/");
			document.getElementById(td_id_arr[index]).innerHTML = value;
			//document.write(td_id_arr[index] + ":" + value+"/");
		});

		$("#ready_state_id").hide();
		$("#show_state_id").show();

		$("#table_div").css({ 'display': "inline-block" });
	}

	// 검색-네이버 클릭 시
	$("#search_naver").click(function(){
    if($('#keyword_table_id tr').size()!=0&&portal_type=="/naver"&&path_type=='/search'){
      $('#keyword_table_id tr').remove();
      $('#keyword_div').hide();
    }
    else{
      path_type="/search";
      portal_type="/naver";
      get_detail_data();
    }
	});
	// 검색-다음 클릭 시
	$("#search_daum").click(function(){
    if($('#keyword_table_id tr').size()!=0&&portal_type=="/daum"&&path_type=='/search'){
      $('#keyword_table_id tr').remove();
      $('#keyword_div').hide();
    }
    else{
      path_type="/search";
      portal_type="/daum";
      get_detail_data();
    }
	});
	// 검색-구글 클릭 시
	$("#search_google").click(function(){
    if($('#keyword_table_id tr').size()!=0&&portal_type=="/google"&&path_type=='/search'){
      $('#keyword_table_id tr').remove();
      $('#keyword_div').hide();
    }
    else{
      path_type="/search";
      portal_type="/google";
      get_detail_data();
    }
	});

  // 카페-네이버 클릭 시
  $("#cafe_naver").click(function(){
    if($('#keyword_table_id tr').size()!=0&&portal_type=="/naver"&&path_type=='/cafe'){
      $('#keyword_table_id tr').remove();
      $('#keyword_div').hide();
    }
    else{
      path_type="/cafe";
      portal_type="/naver";
      get_detail_data();
    }
  });
  // 카페-다음 클릭 시
  $("#cafe_daum").click(function(){
    if($('#keyword_table_id tr').size()!=0&&portal_type=="/daum"&&path_type=='/cafe'){
      $('#keyword_table_id tr').remove();
      $('#keyword_div').hide();
    }
    else{
      path_type="/cafe";
      portal_type="/daum";
      get_detail_data();
    }
  });

  // 블로그-네이버 클릭 시
  $("#blog_naver").click(function(){
    if($('#keyword_table_id tr').size()!=0&&portal_type=="/naver"&&path_type=='/blog'){
      $('#keyword_table_id tr').remove();
      $('#keyword_div').hide();
    }
    else{
      path_type="/blog";
      portal_type="/naver";
      get_detail_data();
    }
  });
  // 블로그-다음 클릭 시
  $("#blog_daum").click(function(){
    if($('#keyword_table_id tr').size()!=0&&portal_type=="/daum"&&path_type=='/blog'){
      $('#keyword_table_id tr').remove();
      $('#keyword_div').hide();
    }
    else{
      path_type="/blog";
      portal_type="/daum";
      get_detail_data();
    }
  });




    // document.getElementById("test_btn").addEventListener("click", function(event){
    // 	document.body.style.background = "red";
    // });

    // document.getElementById("month_id").addEventListener("click", function(event){
    //     $("#main_section").load("user_index_stats/month.html");
    // });




 