    var glob_year;
    var glob_month;
    var year_month_flag="";

    var rootURL = "";   

    var add_url_string="";

    //페이지 최초 진입시
    //아무것도 선택하지 않은 경우
    var selected_year = $("#year_select").val();
    var selected_month = $("#month_select").val();
    glob_year = selected_year;
    glob_month = selected_month;
    if($('#year_select').val()=='년도' && $('#month_select').val()=='월'){
      $("#before_select_year").show();
      $("#after_select_year").hide();
    }
    else if($('#year_select').val()=="년도" && $('#month_select').val()!="월"){
      //년 없이 월만 선택한 경우
      $("#before_select_year").show();
      $("#after_select_year").hide();
    }
    else if($('#year_select').val()!="년도" && $('#month_select').val()=="월"){
      //년도만 선택한 경우
      add_url_string="/"+$('#year_select').val()+"/"+"no";
      year_month_flag="year";
      get_periodData();
      $("#before_select_year").hide();
      $("#after_select_year").show();
    }
    else if($('#year_select').val()!="년도" && $('#month_select').val()!="월"){
      //년도와 월 모두 선택한 경우
      add_url_string="/"+$('#year_select').val()+"/"+$('#month_select').val();
      year_month_flag="month";
      get_periodData();
      $("#before_select_year").hide();
      $("#after_select_year").show();
    }

    // select box에서 년도 및 월 선택시
    var id=0;
    document.getElementById('year_select').onchange = function(){y_m_change()};
    document.getElementById('month_select').onchange = function(){y_m_change()};
    function y_m_change(){
    if($('#bar_chart_id').length){
      $("#ready_state_id").show();
      $("#show_state_id").hide();
      var selected_year = $("#year_select").val();
      var selected_month = $("#month_select").val();
      glob_year = selected_year;
      glob_month = selected_month;

      if(selected_year=="년도" && selected_month=="월"){
        $("#before_select_year").show();
        $("#after_select_year").hide();
      }
      else if(selected_year=="년도" && selected_month!="월"){
        //년 없이 월만 선택한 경우
        $("#before_select_year").show();
        $("#after_select_year").hide();
      }
      else if(selected_year!="년도" && selected_month=="월"){
        //년도만 선택한 경우
        add_url_string="/"+$('#year_select').val()+"/"+"no";
        year_month_flag="year";
        get_periodData();
      }
      else if(selected_year!="년도" && selected_month!="월"){
        //년도와 월 모두 선택한 경우
        add_url_string="/"+$('#year_select').val()+"/"+$('#month_select').val();
        year_month_flag="month";
        get_periodData();
      }
      $("#before_select_year").hide();
      $("#after_select_year").show();
  }
}



    function get_periodData(){
    $.ajax({
      type: 'GET',
      // url: rootURL+'/period',
      url: rootURL+'/period'+add_url_string, //최초생성시에는 최초의 glob_year(=2016)년 데이터 표출하겠다
      dataType: "json",
      success: gen_periodBar_chart
      });
    }

    // 년도별 bar chart 생성
    function gen_periodBar_chart(data){
      if(year_month_flag=='year'){
        var list = data == null ? [] : (data.period instanceof Array ? data.period : [data.period]);
        $("#ready_state_id").hide();
        $("#show_state_id").show();
        var dataPoints_obj=[];
        var i=0;
        for(i=0; i<list.length; i++){
          dataPoints_obj.push({ label: (i+1)+"월", y: list[i] });
        }
        var chart = new CanvasJS.Chart("bar_chart_id", {
          theme: "theme2",
          title:{
            text: glob_year+"년 (월별)",
            fontSize: 18
          },
          data: [  //array of dataSeries     
          { //dataSeries - first quarter
          /*** Change type "column" to "bar", "area", "line" or "pie"***/    
           type: "column",
           name: "접속자 수",
           color: "#FF8224",
           showInLegend: true,
           dataPoints: dataPoints_obj,
           click: function(e){
            glob_month = e.dataPoint.x+1;
            add_url_string="/"+$('#year_select').val()+"/"+(e.dataPoint.x+1);
            year_month_flag="month";
            get_periodData();
           }
          },
        // }
        ],
        /** Set axisY properties here*/
        axisY:{
          //prefix: "$",
          suffix: "명"
        },
        axisX:{
          //prefix: "$",
        }
        });
        chart.render();

        //테이블 채우는 부분
        var tr_label = document.getElementById('table_label_id');
        var tr_value = document.getElementById('table_value_id');
        while (tr_label.hasChildNodes()||tr_value.hasChildNodes()) {
          tr_label.removeChild(tr_label.lastChild);
          tr_value.removeChild(tr_value.lastChild);
        }
        // document.write(list.length);
        var max_td_value=0; var max_td_idx=0;
        var min_td_value=Number.MAX_VALUE; var min_td_idx=0;
        for(var i=0; i<list.length; i++){
          var td_label = document.createElement('td');
          var td_value = document.createElement('td');
          td_label.innerHTML=(i+1);
          td_value.innerHTML=list[i];
          tr_label.appendChild(td_label);
          tr_value.appendChild(td_value);
          // 최대값과 인덱스 찾기
          if(list[i]>max_td_value){
            max_td_idx=i;
            max_td_value=list[i];
          }
          // 최소값과 인덱스 찾기
          if(list[i]<min_td_value&&list[i]!=0){
            min_td_idx=i;
            min_td_value=list[i];
          }
        }
        tr_value.childNodes[max_td_idx].style.backgroundColor="#B2CCFF";
        tr_value.childNodes[min_td_idx].style.backgroundColor="#FFA7A7";
        tr_label.style.backgroundColor="#FF8224";
        //모든 td 길이들 max_td_width로 통일시켜주는 과정
        var max_td_width = 0;    
        $('td').each(function() {
          max_td_width = Math.max($(this).width(), max_td_width);
        }).width(max_td_width);
      }




      else if(year_month_flag=='month'){
        var list = data == null ? [] : (data.period instanceof Array ? data.period : [data.period]);

        $("#ready_state_id").hide();
        $("#show_state_id").show();
        
        var dataPoints_obj=[];
        var i=0;
        for(i=0; i<list.length; i++){
          dataPoints_obj.push({ label: (i+1)+"일", y: list[i] });
        }
        // var dataPoints_obj = [
        //  { label: '31', y: list[30] }
        //  ];


        var chart = new CanvasJS.Chart("bar_chart_id", {

          theme: 'theme2',

          title:{
            text: glob_year+'년 '+glob_month+'월 (일별)',
            fontSize: 18         
          },

          data: [  //array of dataSeries     
          { //dataSeries - first quarter
           /*** Change type 'column' to 'bar', 'area', 'line' or 'pie'***/        
           type: 'column',
           name: '접속자 수',
           color: '#4374D9',
           showInLegend: true,
           dataPoints: dataPoints_obj
          },
          ],
          /** Set axisY properties here*/
          axisY:{
            //prefix: '$',
            suffix: '명'
          },
          axisX:{
            //prefix: '$',
          }
        });
        chart.render();
        //테이블 채우는 부분
        var tr_label = document.getElementById('table_label_id');
        var tr_value = document.getElementById('table_value_id');
        while (tr_label.hasChildNodes()||tr_value.hasChildNodes()) {
          tr_label.removeChild(tr_label.lastChild);
          tr_value.removeChild(tr_value.lastChild);
        }
        var max_td_value=0; var max_td_idx=0;
        var min_td_value=Number.MAX_VALUE; var min_td_idx=0;
        for(var i=0; i<list.length; i++){
          var td_label = document.createElement('td');
          var td_value = document.createElement('td');
          td_label.innerHTML=(i+1);
          td_value.innerHTML=list[i];
          tr_label.appendChild(td_label);
          tr_value.appendChild(td_value);
          // 최대값과 인덱스 찾기
          if(list[i]>max_td_value){
            max_td_idx=i;
            max_td_value=list[i];
          }
          // 최소값과 인덱스 찾기
          if(list[i]<min_td_value){
            min_td_idx=i;
            min_td_value=list[i];
          }
        }
        tr_value.childNodes[max_td_idx].style.backgroundColor="#B2CCFF";
        tr_value.childNodes[min_td_idx].style.backgroundColor="#FFA7A7";
        tr_label.style.backgroundColor="#4374D9";
        //모든 td 길이들 max_td_width로 통일시켜주는 과정
        var max_td_width = 0;    
        $('td').each(function() {
          max_td_width = Math.max($(this).width(), max_td_width);
        }).width(max_td_width);
      }
    }
